#!/usr/bin/python3
#
# set ts=4
#

import sys
import os

from distutils.core import setup, Extension
has_setuptools = False
try:
    from setuptools import setup, Extension
    has_setuptools = True
except ImportError:
    from distutils.core import setup, Extension

kwargs = dict()
if has_setuptools:
  kwargs = dict(
    include_package_data = True,
    install_requires = ['setuptools'],
    zip_safe = False
  )

setup ( name = 'pxeconfig',
    version = '5.0.0',
    description = 'SURF pxeconfig utilities',
    author = 'Bas van der Vlies',
    author_email = 'bas.vandervlies@surf.nl',
    url = 'https://gitlab.com/surfsara/pxeconfig',


    extra_path = 'pxeconfig',
    package_dir = { '' : 'src' },
    py_modules = [ 'pxeconfig', 'pxeconfigd', 'pxe_global', 'AdvancedParser' ],
)

%{!?python3_sitearch: %define python3_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(0)")}

### Abstract ###

Name: pxeconfig
Version: 5.0.0
Release: 1%{?dist}
License: See LICENSE
Group: Development/Libraries
Summary: This package contains PXE config
URL: https://subtrac.sara.nl/oss/pxeconfig
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
Source: https://gitlab.com/surfsara/pxeconfig/-/archive/5.0.0/pxeconfig-5.0.0.tar.gz


%description
This package contains PXE config

%prep
%setup -q -n pxeconfig-%{version}

%define _lib lib
%configure
%build
make compile

%install
make DESTDIR=$RPM_BUILD_ROOT install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,0755)
%{_bindir}/*
%{_sbindir}/*
%{_prefix}/share/doc/pxeconfig/*
%{python3_sitearch}/pxeconfig.pth
%{python3_sitearch}/pxeconfig/*
%config /etc/pxeconfig.conf

%changelog
* Tue Mar 21 08:37:11 CET 2023 Ole Holm Nielsen <Ole.H.Nielsen@fysik.dtu.dk>
- First attempt to make a working .spec file
- Changed python_sitearch into python3_sitearch
- Fixed %changelog date
* Wed Apr 14 2010 Ramon Bastiaans <ramon.bastiaans@sara.nl>
- First .spec file
